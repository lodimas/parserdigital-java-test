# Parser Digital Java Test

Java & Angular simple application for the Java PArser Java Test.

## Base Software

* Java SDK 8: 8.0.241
* Java SDK 13: 13.0.2
* Maven: 3.6.3
* Eclipse: 2019.12
    * Lombok: 1.1.8.10
    * Spring Tools: 4.5.1
* NodeJS: 13.7.0
* WebStorm: 2019.3.2
* SoapUI: 5.5.0
